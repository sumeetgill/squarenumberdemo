package com.sumeetgill.squarenumberdemo;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void isSquare_isCorrect() {

        MainActivity test = new MainActivity();

        assertFalse(test.isSquare(17));
        assertFalse(test.isSquare(40));

        assertTrue(test.isSquare(4));
        assertTrue(test.isSquare(9));
        assertTrue(test.isSquare(16));
    }

    @Test
    public void isTriangular_isCurrect() {

        MainActivity test = new MainActivity();

        assertTrue(test.isTriangular(1));
        assertTrue(test.isTriangular(3));
        assertTrue(test.isTriangular(6));
        assertTrue(test.isTriangular(10));
        assertTrue(test.isTriangular(15));
        assertTrue(test.isTriangular(21));

        assertFalse(test.isTriangular(2));
        assertFalse(test.isTriangular(7));
        assertFalse(test.isTriangular(16));
        assertFalse(test.isTriangular(22));
    }
}
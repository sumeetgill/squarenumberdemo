package com.sumeetgill.squarenumberdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    public void calculateNumber(final View view) {

        EditText txtNumber = (EditText) findViewById(R.id.txtNumber);

        // Set up for allowing user to hit enter on keyboard instead of button
        txtNumber.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    view.performClick();
                    return true;
                }
                return false;
            }
        });

        int number = Integer.parseInt(txtNumber.getText().toString());

        boolean isSquare = isSquare(number);
        boolean isTriangular = isTriangular(number);

        Toast.makeText(this, "Square: " + isSquare + "\nTriangular: " + isTriangular, Toast.LENGTH_SHORT).show();
    }

    public boolean isSquare(int number) {

        int squareRoot = (int) Math.sqrt(number);

        if ((squareRoot * squareRoot) == number) {
            return true;
        }

        return false;
    }

    public boolean isTriangular(int number) {

        final long n = (long) Math.sqrt(2 * number);

        if (n * (n + 1) / 2 == number) {
            return true;
        }

        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}